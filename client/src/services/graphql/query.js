import { gql } from '@apollo/client'

export const GET_DETAIL = gql`
query getDetail ($id: ID) {
  getMovieById ( id: $id){
    _id
    title
    overview
    poster_path
    popularity
    tags
  }
}
`;

export const GET_MOVIES_SERIES = gql`
query getAll {
  getMovies{
    _id
    title
    overview
    poster_path
    popularity
    tags
  },
  getSeries{
    _id
    title
    overview
    poster_path
    popularity
    tags
  }
}`

export const GET_MOVIES = gql`
query getAll {
  getMovies{
    _id
    title
    poster_path
    popularity
  }
}`


export const GET_SERIES = gql`
query getAll {
  getSeries{
    _id
    title
    poster_path
    popularity
  }
}`

