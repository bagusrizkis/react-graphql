import { gql } from '@apollo/client'

export const EDIT_MOVIE = gql`
mutation editMovie($id: ID, $title: String, $overview: String, $poster_path: String, $popularity: Float, $tags: [String]) {
  editMovie(
    id: $id,
    title: $title,
    overview: $overview,
    poster_path: $poster_path,
    popularity: $popularity,
    tags: $tags
  ){
    _id
  }
}
`

export const ADD_MOVIE = gql`
mutation addMovie ($title: String, $overview: String, $poster_path: String, $popularity: Float, $tags: [String]) {
  addMovie(
    title: $title
    overview: $overview
    poster_path: $poster_path
    popularity: $popularity
    tags: $tags
  ) {
    _id
    title
    overview
    poster_path
    popularity
    tags
  }
}
`


export const DEL_MOVIE = gql`
mutation deleteMovieById ($id: ID) {
  deleteMovie(id: $id) {
    _id
    title
    overview
    poster_path
    popularity
    tags
  }
}
`