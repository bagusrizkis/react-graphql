import { useQuery } from '@apollo/client'
import ScrollMenu from 'react-horizontal-scrolling-menu';
import { CardComponent, HomeLoadComponent } from '../components'
import { Link } from 'react-router-dom'
import {List} from 'antd'
import { GET_MOVIES_SERIES } from '../services/graphql/query'
import { ArrowLeft, ArrowRight } from '../components/ScrollMenu/Arrow'

const cardList = (data = {}, wrapper) =>
  data[wrapper].map((list) => {
    return <CardComponent cardAction={false} data={list} key={list._id} name='series' />
  })

function HomePage() {
  const { loading, data, error } = useQuery(GET_MOVIES_SERIES)
  console.log(data)

  if (loading) return <HomeLoadComponent />
  if (error) return <p>Error :( {JSON.stringify(error)}</p>;

  return (
    <>
      <h1 className='mainTitle'><Link to='/series'>Tv Series</Link></h1>
      <ScrollMenu
          data={cardList(data, 'getSeries')}
          arrowLeft={ArrowLeft}
          arrowRight={ArrowRight}
          dragging={true}
          wheel={true}
        />
      <h1 className='mainTitle'><Link to='/movies'>Movies</Link></h1>
      <List
        grid={{ gutter: 16, xs: 1, sm: 2, md: 3, lg: 3, xl: 5, xxl: 5 }}
        dataSource={data.getMovies}
        renderItem={item => (
          <List.Item>
            <CardComponent data={item} key={item._id} width={310} name='movies' cardAction={true} />
          </List.Item>
        )}
      />,
    </>
  )
}

export default HomePage