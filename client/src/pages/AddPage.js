import React from 'react'
import { Image, Input, Col, Row, Typography, Divider, Button, Slider, InputNumber } from 'antd';
import { HighlightOutlined } from '@ant-design/icons';
import CreatableSelect from 'react-select/creatable';
import { useState } from 'react';
import { ADD_MOVIE } from '../services/graphql/mutation';
import { useMutation } from '@apollo/client';
import { useHistory } from 'react-router-dom'
import client from '../config/graphql'
import { GET_MOVIES_SERIES } from '../services/graphql/query';
import { Notification } from '../components'
const { Title, Paragraph } = Typography;

function ManagePage() {
  const [title, setTitle] = useState('Movie Title')
  const [posterPath, setPosterPath] = useState('')
  const [popularity, setPopularity] = useState(0)
  const [overview, setOverview] = useState('Write Movie Overview')
  const [tags, setTags] = useState({
    rawTags: [],
    inputValue: '',
    value: [{ label: 'Input Tags', value: 'Input Tags' }]
  })

  const handleChange = (value, actionMeta) => {
    if (!value) value = []
    setTags({
      inputValue: '',
      value: value,
      rawTags: value.map(function (el) { return el.value })
    });
  };

  const handleKeyDown = (event) => {
    const { inputValue, value, rawTags } = tags;
    if (!inputValue) return;
    switch (event.key) {
      case 'Enter':
      case 'Tab':
        setTags({
          inputValue: '',
          value: [...value, { label: inputValue, value: inputValue }],
          rawTags: [...rawTags, inputValue]
        });
        event.preventDefault();
        // console.log(tags)
        break
      default:
      // console.log('event', event, inputValue, value)
      // console.log(tags)
    }
  };
  const history = useHistory()
  const [ addMovie ] = useMutation(ADD_MOVIE, {
    onCompleted: (newData) => {
      let {getMovies} = client.readQuery({
        query: GET_MOVIES_SERIES
      })
      client.writeQuery({
        query: GET_MOVIES_SERIES,
        data: {
          getMovies: [...getMovies, newData.addMovie]
        }
      })
      Notification({
        type: 'success',
        message: 'Success Add',
        description: `${newData.addMovie.title} Was Added`
        })
      history.push('/movies/' + newData.addMovie._id)
    }
  })
  const handleButtonSave = () => {
    console.log('masuk save', title, posterPath, popularity, overview, tags.rawTags)
    addMovie({ variables: {
      title,
      poster_path: posterPath,
      popularity,
      tags: tags.rawTags,
      overview
    }})
  }

  const handleInputChange = (inputValue) => {
    setTags({ ...tags, inputValue: inputValue });
  };

  useState(() => {
    console.log('masuk page', tags.value)
  }, [])

  const handlePoster = ({target}) => {
    setPosterPath(target.value)
  }

  return (
    <div style={{ paddingTop: '2rem', paddingBottom: '2rem' }}>
      <Row style={{ minHeight: '86vh' }}>
        <Col xs={2} sm={4} md={6} lg={12} xl={8}>
          <div style={posterPath === '' ? { maxWidth: '25vw', height: '80vh', maxHeight: '80vh', display: 'flex', alignItems: 'center', justifyContent: 'center', border: '#278ea5 0.3rem dashed' }: {}}>
            { 
              posterPath 
              ? <Image
              width={'30rem'}
              src={posterPath}
              />
              : <></>
            }
          </div>
          <div>
            <Input onChange={handlePoster} placeholder="Link Poster" value={posterPath} /> 
          </div> 
        </Col>
        <Col xs={2} sm={4} md={6} lg={12} xl={16}>
          <div>
            <Title
              style={{ color: '#278ea5' }}
              editable={{
                icon: <HighlightOutlined />,
                tooltip: 'click to input movie title',
                onChange: setTitle,
              }}
            >{title}</Title>
            <div>
              <Divider orientation="left" style={{ color: "#278ea5" }} >Category:</Divider>
              <CreatableSelect
                components={{ DropdownIndicator: null }}
                inputValue={tags.inputValue}
                isClearable
                isMulti
                menuIsOpen={false}
                onChange={handleChange}
                onInputChange={handleInputChange}
                onKeyDown={handleKeyDown}
                placeholder="Type something and press enter..."
                value={tags.value}
              />
            </div>
            <div>
              <Divider orientation="left" style={{ color: "#278ea5" }} >Popularity:</Divider>
              <Row>
                <Col span={12}>
                  <Slider
                    min={1.0}
                    max={10.0}
                    step={0.1}
                    onChange={(num) => setPopularity(num)}
                    value={typeof popularity === 'number' ? popularity : 0}
                  />
                </Col>
                <Col span={4}>
                  <InputNumber
                    min={1.0}
                    max={10.0}
                    step={0.1}
                    style={{ margin: '0 16px' }}
                    value={popularity}
                    onChange={(num) => setPopularity(num)}
                  />
                </Col>
              </Row>
            </div>
            <div>
              <Divider orientation="left" style={{ color: "#278ea5" }} >Overview:</Divider>
              <Paragraph
                copyable
                style={{ fontSize: '1.2rem', color: "#278ea5", borderColor: 'white' }}
                editable={{
                  icon: <HighlightOutlined />,
                  tooltip: 'click to input movie overview',
                  onChange: setOverview,
                }}
              >{overview}
              </Paragraph>
            </div>
            <div>
              <Button
                type='primary'
                shape="round"
                onClick={() => handleButtonSave()}
              >Save Data
              </Button>
            </div>
          </div>
        </Col>
      </Row>
    </div>
  )
}

export default ManagePage