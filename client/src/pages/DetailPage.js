import { useMutation, useQuery } from '@apollo/client'
import { Col, Image, Row, Tag, Typography, Divider, Button, Input, Slider, InputNumber } from 'antd';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { EDIT_MOVIE } from '../services/graphql/mutation';
import CreatableSelect from 'react-select/creatable';
import { StarOutlined } from '@ant-design/icons';
import { Notification } from '../components'
const { Title, Paragraph } = Typography;
const { GET_DETAIL, GET_MOVIES_SERIES } = require('../services/graphql/query')


function DetailPage() {
  const [editData, setEditData] = useState(false)
  const [dataBackUp, setDataBackUp] = useState({})
  const [dataLocal, setDataLocal] = useState({
    title: '',
    poster_path: '',
    tags: [],
    overview: '',
    popularity: 0
  })
  const [inputTag, setInputTag] = useState({
    inputValue: '',
    value:[]
  })

  const params = useParams()
  const [ editMovie ] = useMutation(EDIT_MOVIE, {
    refetchQueries: [{query: GET_MOVIES_SERIES}],
    onCompleted: (newData) => {
      Notification({
        type: 'success',
        message: 'Success Edit Movie',
        description: `${dataLocal.title} Was Edited`
      })
    }
  })
  const { data, loading, error } = useQuery(GET_DETAIL, {
    variables: {
      id: params.id
    }
  })

  useEffect(() => {
    if (!loading) {
      setDataLocal(data.getMovieById)
      setDataBackUp(data.getMovieById)
      setInputTag({...inputTag, value: data.getMovieById.tags.map(function (el) {return {value: el, label: el}})})
    }
     // eslint-disable-next-line
  }, [data, loading])

  const handleButtonEdit = () => {
    setEditData(!editData)
    if (editData) {
      console.log('dari edited data', editData)
      setDataLocal(dataBackUp)
      setInputTag({...inputTag, value: dataBackUp.tags.map(function (el) {return {value: el, label: el}})})
    }
  }

  const handleButtonSave = () => {
    editMovie({ variables: {
      id: params.id,
      title: dataLocal.title,
      poster_path: dataLocal.poster_path,
      popularity: dataLocal.popularity,
      tags: dataLocal.tags,
      overview: dataLocal.overview
    }})
    setEditData(false)
  }

  const handleChange = (value, actionMeta) => {
    if (!value) value = []
    setInputTag({
      inputValue: '',
      value: value
    })
    setDataLocal({...dataLocal, tags: value.map(function (el) { return el.value })})
  };

  const handleKeyDown = (event) => {
    const { inputValue, value } = inputTag;
    if (!inputValue) return;
    switch (event.key) {
      case 'Enter':
      case 'Tab':
        setInputTag({
          inputValue: '',
          value: [...value, { label: inputValue, value: inputValue }],
        });
        setDataLocal({...dataLocal, tags: [...dataLocal.tags, inputValue]})
        event.preventDefault();
        break
      default:
    }
  };

  const handleInputChange = (inputValue) => {
    setInputTag({ ...inputTag, inputValue: inputValue });
  };

  if (loading) return <h1>Loading..</h1>
  if (error) return <p>Error :( {JSON.stringify(error)}</p>

  return (
    <div style={{ paddingTop: '2rem', paddingBottom: '2rem' }}>
      <Row style={{ minHeight: '86vh' }}>
        <Col xs={24} sm={24} md={24} lg={12} xl={8}>
          <div style={{}}>
            <Image
              width={'30rem'}
              src={dataLocal.poster_path}
            />
            { editData
              ? <div> <Input placeholder="Link Poster" onChange={(value) => setDataLocal({...dataLocal, poster_path: value})} value={dataLocal.poster_path} /> </div> 
              : <></>
            }
          </div>
        </Col>
        <Col xs={24} sm={24} md={24} lg={12} xl={16}>
          <div>
            <Title
              style={{ color: '#278ea5' }}
              editable={editData ? {
                tooltip: 'click to edit text',
                onChange: (value) => setDataLocal({ ...dataLocal, title: value })
              } : false}
            >{dataLocal.title}
            </Title>
            <div>
              <Divider orientation="left" style={{ color: "#278ea5" }} >Category:</Divider>
              {
                !editData
                ? dataLocal.tags.map(tag =>
                  <Tag style={{ padding: '0.4rem 1.5rem', fontSize: '1rem', height: '100%', borderRadius: '0.4rem' }} color={'#278ea5'}>{tag}</Tag>
                )
                : <CreatableSelect
                components={{ DropdownIndicator: null }}
                inputValue={inputTag.inputValue}
                isClearable
                isMulti
                menuIsOpen={false}
                onChange={handleChange}
                onInputChange={handleInputChange}
                onKeyDown={handleKeyDown}
                placeholder="Type something and press enter..."
                value={inputTag.value}
              />
              }
            </div>
            <div>
            <Divider orientation="left" style={{ color: "#278ea5" }} >Popularity:</Divider>
            {
              !editData ? <div style={{display: "flex", color: "#278ea5"}}>< StarOutlined size='large' style={{margin: '3px 1rem'}}/> <p> {dataLocal.popularity}</p></div>
              : <Row>
                <Col span={12}>
                  <Slider
                    min={1.0}
                    max={10.0}
                    step={0.1}
                    onChange={(num) => setDataLocal({...dataLocal, popularity: num})}
                    value={typeof dataLocal.popularity === 'number' ? dataLocal.popularity : 0}
                  />
                </Col>
                <Col span={4}>
                  <InputNumber
                    min={1.0}
                    max={10.0}
                    step={0.1}
                    style={{ margin: '0 16px' }}
                    value={dataLocal.popularity}
                    onChange={(num) => setDataLocal({...dataLocal, popularity: num})}
                  />
                </Col>
              </Row>
              }
            </div>
            <div>
              <Divider orientation="left" style={{ color: "#278ea5" }} >Overview:</Divider>
              <Paragraph
                editable={editData ? {
                  tooltip: 'click to edit text',
                  onChange: (value) => setDataLocal({ ...dataLocal, overview: value })
                } : false}
                copyable
                style={{ fontSize: '1.2rem', color: "#278ea5", borderColor: 'white' }}
              >{dataLocal.overview}
              </Paragraph>
            </div>
            <div>
              <Button
                type='primary'
                shape="round"
                onClick={() => handleButtonEdit()}
              >{editData ? 'Cancel' : 'Edit Data'}
              </Button>
              { 
                editData
                  ? <Button
                    type='primary'
                    shape="round"
                    onClick={() => handleButtonSave()}
                  >Save Data
                </Button>
                  : <></>
              }
            </div>
          </div>
        </Col>
      </Row>
    </div>
  )
}

export default DetailPage