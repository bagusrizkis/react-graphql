import { CardComponent } from '../components'
import {List} from 'antd'
import { favoritesVars } from '../services/graphql/reactiveVars'
import { useReactiveVar } from '@apollo/client'

function FavoritePage () {
  // const data = favoritesVars()
  const data = useReactiveVar(favoritesVars)
  // console.log(data)



  return (
    <div>
      <div style={{display: 'flex', alignItems: 'center', flexDirection: 'column', marginBottom: "2rem"}}>
      <h1 className='mainTitle' style={{alignSelf: 'flex-start'}}>My Favorite Movies</h1>
      </div>
      <div>
        <List
          grid={{ gutter: 16, xs: 1, sm: 2, md: 3, lg: 3, xl: 5, xxl: 5 }}
          dataSource={data}
          renderItem={item => (
            <List.Item>
              <CardComponent data={item} key={item._id} width={310} name='movies' cardAction={true} />
            </List.Item>
          )}
        />
        </div>
    </div>
  )
}

export default FavoritePage