import { useQuery } from '@apollo/client'
import {List, Button} from 'antd'
import { EditFilled } from '@ant-design/icons';
import { CardComponent } from '../components'
import { GET_MOVIES } from '../services/graphql/query'
import { useHistory } from 'react-router-dom';

function Movie (props) {
  const { loading, data, error } = useQuery(GET_MOVIES)
  const history = useHistory()

  const goToAdd = () => {
    history.push({
      pathname: `/addMovie`
    })
  }

  if (loading) return <h1>Loading..</h1>
  if (error) return <p>Error :( {JSON.stringify(error)}</p>;

  return (
    <div>
      <div style={{display: 'flex', alignItems: 'center', flexDirection: 'column', marginBottom: "2rem"}}>
      <h1 className='mainTitle' style={{alignSelf: 'flex-start'}}>Find The Best Movie</h1>
      <Button onClick={goToAdd} style={{alignSelf: 'flex-end'}} type="primary" shape="round" icon={<EditFilled />} size={20}>
          Add Movie
      </Button>
      </div>
      <div>
        <List
          grid={{ gutter: 16, xs: 1, sm: 2, md: 3, lg: 3, xl: 5, xxl: 5 }}
          dataSource={data.getMovies}
          renderItem={item => (
            <List.Item>
              <CardComponent data={item} key={item._id} width={310} name='movies' cardAction={true} />
            </List.Item>
          )}
        />,
      </div>
    </div>
  )
}

export default Movie