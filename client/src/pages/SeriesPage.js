import { useQuery } from '@apollo/client'
import {List} from 'antd'
import { CardComponent } from '../components'
import { GET_SERIES } from '../services/graphql/query'

function Series (props) {
  const { loading, data, error } = useQuery(GET_SERIES)

  if (loading) return <h1>Loading..</h1>
  if (error) return <p>Error :( {JSON.stringify(error)}</p>;

  return (
    <div>
      <div style={{display: 'flex', alignItems: 'center', flexDirection: 'column', marginBottom: "2rem"}}>
      <h1 className='mainTitle' style={{alignSelf: 'flex-start'}}>Find The Best TV Series</h1>
      </div>
      <div>
        <List
          grid={{ gutter: 16, xs: 1, sm: 2, md: 3, lg: 3, xl: 5, xxl: 5 }}
          dataSource={data.getSeries}
          renderItem={item => (
            <List.Item>
              <CardComponent data={item} key={item._id} width={310} name='series' cardAction={false} />
            </List.Item>
          )}
        />,
      </div>
    </div>
  )
}

export default Series