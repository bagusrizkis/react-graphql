import { ApolloProvider } from '@apollo/client'
import { 
  BrowserRouter as Router,
  Route,
  Switch 
} from 'react-router-dom'
import './css/App.css'
import {
  HomePage,
  MoviePage,
  SeriesPage,
  DetailPage,
  AddPage,
  FavoritesPage
} from './pages'
import client from './config/graphql';
import { NavbarComponent } from './components'
import { Layout } from 'antd';
const { Content } = Layout;

function App() {
  return (
    <ApolloProvider client={client}>
      <Layout className="layout">
        <Router>
          <NavbarComponent id='top-navigation' />
          <Content style={{ padding: '0 50px', backgroundColor: '#001529' }}>
            <Switch>
              <Route exact path="/" component={HomePage} />
              <Route path="/movies/:id" component={DetailPage} />
              <Route path="/series/:id" component={DetailPage} />
              <Route exact path="/movies" component={MoviePage} />
              <Route exact path="/series" component={SeriesPage} />
              <Route exact path="/addMovie" component={AddPage} />
              <Route exact path="/favorites" component={FavoritesPage} />
            </Switch>
          </Content>
        </Router>
      </Layout>
    </ApolloProvider>
  );
}

export default App;
