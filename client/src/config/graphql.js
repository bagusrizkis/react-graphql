import { ApolloClient, InMemoryCache } from '@apollo/client';

const client = new ApolloClient({
  uri: 'http://34.234.66.30:4000/',
  cache: new InMemoryCache()
})

export default client