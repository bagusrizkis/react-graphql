import { Layout, Menu } from 'antd';
import { Link, useLocation } from 'react-router-dom'

const { Header } = Layout;

function Navbar(props) {
  const location = useLocation()

  return (
    <Header>
      <div className="logo"><Link to='/'>Entertain Me</Link></div>
      <Menu
        theme="dark"
        mode="horizontal"
        selectedKeys={location.pathname}
      >
        <Menu.Item key="/series"><Link to='/series'>TV Series</Link></Menu.Item>
        <Menu.Item key="/movies"><Link to='/movies'>Movies</Link></Menu.Item>
        <Menu.Item key="/favorites"><Link to='/favorites'>Favorites</Link></Menu.Item>
      </Menu>
    </Header>
  )
}

export default Navbar