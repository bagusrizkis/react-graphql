import { Card } from 'antd'
import { useHistory } from 'react-router-dom';
import { RestFilled, HeartOutlined, StarOutlined, HeartFilled } from '@ant-design/icons';
import { useMutation, useReactiveVar } from '@apollo/client';
import { DEL_MOVIE } from '../../services/graphql/mutation';
import { GET_MOVIES_SERIES } from '../../services/graphql/query'
import { favoritesVars } from '../../services/graphql/reactiveVars'
import { useEffect, useState } from 'react';
import { Notification } from '../'
const { Meta } = Card;

function CardComponent(props) {
  const favoritesData = useReactiveVar(favoritesVars)
  const [addedFav, setAddedFav] = useState(false)
  const { data, width, name, cardAction } = props
  const history = useHistory()  

  const gotoDetail = () => {
    history.push({
      pathname: `${name}/${data._id}`,
      state: { usageFor: 'getMoviesById' }
    })
  }

  useEffect(() => {
    let check = favoritesData.find(mv => mv._id === data._id)
    console.log('>>>>', cardAction)
    if (!check) { setAddedFav(false) }
    else setAddedFav(true)
    // eslint-disable-next-line
  }, [favoritesData])

  const removeFav = () => {
    let check = favoritesData.filter(mv => mv._id !== data._id)
    // console.log('++++', check)
    favoritesVars(check)
    Notification({
      type: 'success',
      message: 'Success Delete From favorites',
      description: `${data.title} Was Deleted from favorites`
    })
  }

  const [deleteMutation] = useMutation(DEL_MOVIE, {
    refetchQueries: [{query: GET_MOVIES_SERIES}],
    onCompleted: (data) => {
      Notification({
      type: 'success',
      message: 'Success Delete',
      description: `${data.deleteMovie.title} Was Deleted`
      })
    }
  })
  const deleteHanlde = () => {
    deleteMutation({variables: {id: data._id}})
  }

  const favHandle = () => {
    const favData = favoritesVars()
    favoritesVars([...favData, data])
    console.log('masuk')
    setAddedFav(true)
    Notification({
      type: 'success',
      message: 'Success Add To favorites',
      description: `${data.title} Was Added to favorites`
    })
  }

  return (
    <Card
      hoverable
      style={{ width: width || 190, margin: 5, borderRadius: 10,overflow: 'hidden' }}
      cover={<img style={{}} alt={data.title} src={data.poster_path} onClick={gotoDetail}/>}
      actions={cardAction ? [
        <RestFilled key="delete" onClick={deleteHanlde}/>,
        !addedFav ? <HeartOutlined key="addToFav" onClick={favHandle}/> : <HeartFilled key="addToFav" onClick={removeFav}/>
      ] : {}}
    >
      <Meta
        onClick={gotoDetail}
        style={{padding: 0}}
        title={data.title}
        description={<div style={{display: "flex"}}>< StarOutlined style={{margin: '3px 1rem'}}/> <p> {data.popularity}</p></div>}
      />
    </Card>
  )
}

export default CardComponent