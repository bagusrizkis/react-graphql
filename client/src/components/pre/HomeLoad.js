import ScrollMenu from 'react-horizontal-scrolling-menu';
import { Link } from 'react-router-dom'
import {Card, List} from 'antd'
import { ArrowLeft, ArrowRight } from '../ScrollMenu/Arrow'


const cardList = (data = {}) =>
data.map((item) => {
  return <Card
  key={item}
  style={{ width: 190, margin: 5, borderRadius: 10,overflow: 'hidden' }}
  loading={true}
  ></Card>
})

function HomeLoad() {
  const data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
  return (
    <>
      <h1 className='mainTitle'><Link to='/series'>Tv Series</Link></h1>
      <ScrollMenu
          data={cardList(data)}
          arrowLeft={ArrowLeft}
          arrowRight={ArrowRight}
          dragging={true}
          wheel={true}
        />
      <List
        grid={{ gutter: 16, xs: 1, sm: 2, md: 3, lg: 3, xl: 5, xxl: 5 }}
        dataSource={data}
        renderItem={item => (
          <List.Item>
            <Card
              key={item}
              style={{ width: 310, margin: 5, borderRadius: 10,overflow: 'hidden' }}
              loading={true}
            ></Card>
          </List.Item>
        )}
      />,
    </>
  )
}

export default HomeLoad