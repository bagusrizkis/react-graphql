module.exports = {
  apps: [
    {
      name: 'entertainme - Client',
      script: 'cd client && yarn install && yarn start',
    },
    {
      name: 'entertainme - Orchestrator',
      script: 'cd server/orchestrator/graphql && npm install && nodemon app.js',
    },
    {
      name: 'entertainme - Service Movies',
      script: 'cd server/services/movies && npm install && nodemon app.js',
      env: {
        DATABASE_NAME: "EntertainMe",
        COLLECTION_NAME: "Movies"
      },
    },
    {
      name: 'entertainme - Service TV Series',
      script: 'cd server/services/series && npm install && nodemon app.js',
      env: {
        DATABASE_NAME: "EntertainMe",
        COLLECTION_NAME: "TvSeries"
      },
    },
  ],
};