const router = require('express').Router()
const Controller = require('../controllers/index')


router.get('/', (req, res, next) => {
  res.send('<h1>Server On</h1>')
})

router.get('/movies/', Controller.getMovie)
router.get('/movies/:id', Controller.getDetailMovie)
router.post('/movies/', Controller.addMovie)
router.put('/movies/:id', Controller.editMovie)
router.delete('/movies/:id', Controller.deleteMovie)

module.exports = router