const { ObjectId } = require('mongodb');
const db = require('../config/db.config')
let collection = process.env.COLLECTION_NAME || "TvSeries"
const MoviesCollection = db.collection(collection);

class Movie {
  static async getMovies () {
    try {
      const data = await MoviesCollection.find({}).toArray()
      return data
    } catch (error) {
      return error
    }
  }

  static async getOneMovie (id) {
    try {
      const data = await MoviesCollection
        .findOne({'_id': ObjectId(id)})
      return data
    } catch (error) {
      return error
    }
  }

  static async addMovie (payload) {
    try {
      const data = await MoviesCollection
        .insertOne(payload)
      return data
    } catch (error) {
      return error
    }
  }

  static async editMovie ({id, payload}) {
    try {
      const data = await MoviesCollection
        .findOneAndUpdate({'_id': ObjectId(id)}, {
          $set: payload
        }, {
          returnOriginal: false
        })
      return data
    } catch (error) {
      return error
    }
  }

  static async deleteMovie (id) {
    try {
      const data = await MoviesCollection
        .findOneAndDelete({'_id': ObjectId(id)}, {
          returnOriginal: true
        })
      return data
    } catch (error) {
      return error
    }
  }

}

module.exports = Movie