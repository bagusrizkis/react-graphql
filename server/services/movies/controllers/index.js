const Movie = require('../models/Movie')

class Controller {
  static async getMovie (req, res) {
    const result = await Movie
      .getMovies()
    if (result.name === 'MongoError') {
      return res.status(500).json({
        message: 'database Error',
        error: result
      })
    }
    return res.status(200).json({
      message: 'success find all data',
      result: result
    })
  }

  static async addMovie (req, res) {
    const result = await Movie
      .addMovie({
        title: req.body.title,
        overview: req.body.overview,
        poster_path: req.body.poster_path,
        popularity: req.body.popularity,
        tags: req.body.tags
      })
    return res.status(200).json({
      message: 'success add',
      result: result.ops[0]
    })
  }

  static async getDetailMovie (req, res) {
    const { id } = req.params
    const result = await Movie
      .getOneMovie(id)
    console.log(result)
    if (result.result === null) {
      return res.status(404).json({
        message: 'data tidak ditemukan',
        result: result
      })
    }
    return res.status(200).json({
      message: 'success get detail',
      result: result
    })
  }

  static async editMovie (req, res) {
    const { id } = req.params
    const result = await Movie
      .editMovie({id: id, payload: {
        title: req.body.title,
        overview: req.body.overview,
        poster_path: req.body.poster_path,
        popularity: req.body.popularity,
        tags: req.body.tags
      }})
    if (result.value === null) {
      return res.status(404).json({
        message: 'data tidak ditemukan',
        result: result.value
      })
    }
    return res.status(200).json({
      message: 'success edit',
      result: result.value
    })
  }

  static async deleteMovie (req, res) {
    const { id } = req.params
    const result = await Movie.deleteMovie(id)
    if (result.value === null) {
      return res.status(404).json({
        message: 'data tidak ditemukan',
        result: result
      })
    }
    return res.status(200).json({
      message: 'success delete',
      result: result.value
    })
  }
}

module.exports = Controller