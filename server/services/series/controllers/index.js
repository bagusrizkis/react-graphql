const Series = require('../models/Series')

class Controller {
  static async getSeries (req, res) {
    const result = await Series.getSeries()
    if (result.name === 'MongoError') {
      return res.status(500).json({
        message: 'database Error',
        error: result
      })
    }
    return res.status(200).json({
      message: 'success find all data',
      dataType: typeof (result),
      result: result
    })
  }

  static async addSeries (req, res) {
    const result = await Series.addSeries({
      title: req.body.title,
      overview: req.body.overview,
      poster_path: req.body.poster_path,
      popularity: req.body.popularity,
      tags: req.body.tags
    })
    console.log(result)
    return res.status(200).json({
      message: 'success add',
      dataType: typeof (result.ops[0]),
      result: result.ops[0]
    })
  }

  static async getDetailSeries (req, res) {
    const { id } = req.params
    const result = await Series.getOneSeries(id)
    if (result === null) {
      return res.status(404).json({
        message: 'data tidak ditemukan',
        result: result
      })
    }
    return res.status(200).json({
      message: 'success get detail',
      dataType: typeof (result),
      result: result
    })
  }

  static async editSeries (req, res) {
    const { id } = req.params
    const result = await Series.editSeries({id: id, payload: {
      title: req.body.title,
      overview: req.body.overview,
      poster_path: req.body.poster_path,
      popularity: req.body.popularity,
      tags: req.body.tags
    }})
    return res.status(200).json({
      message: 'success edit',
      dataType: typeof result.value,
      result: result.value
    })
  }

  static async deleteSeries (req, res) {
    const { id } = req.params
    const result = await Series.deleteSeries(id)
    if (result.value === null) {
      return res.status(404).json({
        message: 'data tidak ditemukan',
        result: result.value
      })
    }
    return res.status(200).json({
      message: 'success delete',
      dataType: typeof (result),
      result: result.value
    })
  }
}

module.exports = Controller