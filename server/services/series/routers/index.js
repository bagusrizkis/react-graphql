const router = require('express').Router()
const Controller = require('../controllers')

router.get('/', (req, res) => {
  res.send('connect')
})

router.get('/series', Controller.getSeries)
router.get('/series/:id', Controller.getDetailSeries)
router.post('/series', Controller.addSeries)
router.put('/series/:id', Controller.editSeries)
router.delete('/series/:id', Controller.deleteSeries)

module.exports = router