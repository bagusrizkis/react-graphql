const express = require('express')
const app = express()
const PORT = process.env.PORT || 3002

// Body Parser Middlewares
app.use(express.urlencoded({extended: true}))
app.use(express.json())

app.use(require('./routers'))

app.listen (PORT, () =>
  console.log('🔥 Server Runnning : http://localhost:' + PORT))