const { ObjectId } = require('mongodb');
const db = require('../config/db.config')
let collection = process.env.COLLECTION_NAME || "TvSeries"
const SeriesCollection = db.collection(collection);

class Series {
  static async getSeries () {
    try {
      const data = await SeriesCollection
        .find({}).toArray()
      return data
    } catch (error) {
      return error
    }
  }

  static async getOneSeries (id) {
    try {
      const data = await SeriesCollection
        .findOne({'_id': ObjectId(id)})
      return data
    } catch (error) {
      return error
    }
  }

  static async addSeries (payload) {
    try {
      const data = await SeriesCollection
        .insertOne(payload)
      return data
    } catch (error) {
      return error
    }
  }

  static async editSeries({id, payload}) {
    try {
      const data = await SeriesCollection
        .findOneAndUpdate({'_id': ObjectId(id)}, {
          $set: payload
        }, {
          returnOriginal: false
        })
      return data
    } catch (error) {
      return error
    }
  }

  static async deleteSeries (id) {
    try {
      const data = await SeriesCollection
        .findOneAndDelete({'_id': ObjectId(id)}, {
          returnOriginal: true
        })
      return data
    } catch (error) {
      return error
    }
  }

}

module.exports = Series