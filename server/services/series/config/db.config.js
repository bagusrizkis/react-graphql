const dbName = process.env.DATABASE_NAME || 'EntertainMe'
const uri = "mongodb://localhost:27017/"
const { MongoClient } = require('mongodb');

const client = new MongoClient(uri, { useUnifiedTopology: true })

client.connect();

const db = client.db(dbName);

module.exports = db
