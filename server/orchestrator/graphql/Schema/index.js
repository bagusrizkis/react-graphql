const { gql } = require('apollo-server');

module.exports = gql`
  # Comments in GraphQL strings (such as this one) start with the hash (#) symbol.

  type Movies {
    _id: ID
    title: String,
    overview: String,
    poster_path: String,
    popularity: Float,
    tags: [String]
  }

  type Series {
    _id: ID
    title: String,
    overview: String,
    poster_path: String,
    popularity: Float,
    tags: [String]
  }

  type Query {
    getMovies: [Movies],

    getMovieById(
      id: ID
    ):Movies,

    getSeries: [Series]

    getSeriesById(
      id: ID
    ):Series
  }

  type Mutation {
    # --- Movie

    addMovie(
      title: String,
      overview: String,
      poster_path: String,
      popularity: Float,
      tags: [String]
    ): Movies,

    editMovie(
      id: ID,
      title: String,
      overview: String,
      poster_path: String,
      popularity: Float,
      tags: [String]
    ):Movies,

    deleteMovie(
      id: ID
    ):Movies,

    # --- TV Series

    addSeries(
      title: String,
      overview: String,
      poster_path: String,
      popularity: Float,
      tags: [String]
    ): Series,

    editSeries(
      id: ID,
      title: String,
      overview: String,
      poster_path: String,
      popularity: Float,
      tags: [String]
    ):Series,

    deleteSeries(
      id: ID
    ):Series
  }
`;