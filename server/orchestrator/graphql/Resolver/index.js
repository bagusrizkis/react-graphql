const { 
  moviesUrl, moviesEnd,
  seriesUrl, seriesEnd
} = require('../config/services.url')
const axios = require('axios')
const redis = require('../config/redis.config')
const { ApolloError } = require('apollo-server')

module.exports = {
  Query: {
    // Movies
    async getMovies () {
      console.log('masuk')
      const cacheMovies = await redis
        .get('dataMovies')

      if (!cacheMovies) {
        return axios.get(moviesUrl + moviesEnd)
        .then( async ({ data }) => {
          await redis
            .set('dataMovies', JSON.stringify(data))

          return data.result
        })
        .catch(err => {
          console.error(err)
        })
      }
      return (JSON.parse(cacheMovies)).result
    },

    async getMovieById (_, args) {
      const { id } = args
      console.log('masuk server', id)
      return axios
        .get(moviesUrl + moviesEnd + id)
        .then(async ({ data }) => {
          return data.result
        })
        .catch((err) => {
          console.error(err);
        })
    },

    // TV Series

    async getSeries () {
      const cacheSeries = await redis.get('dataSeries')
      if (!cacheSeries) {
        return axios.get(seriesUrl + seriesEnd)
        .then( async ({ data }) => {
          await redis.set('dataSeries', JSON.stringify(data))
          return data.result
        })
        .catch(err => {
          console.error(err); 
        })
      }
      return (JSON.parse(cacheSeries)).result
    },

    async getSeriesById (_, args) {
      const { id } = args
      return axios
        .get(seriesUrl + seriesEnd + id)
        .then(async ({ data }) => {
          return data.result
        })
        .catch((err) => {
          console.error(err);
        })
    }
  },

  Mutation: {
    addMovie(_, args) {
      const { title, overview, poster_path, popularity, tags } = args

      return axios.post(moviesUrl + moviesEnd, {
        title, overview, poster_path, popularity, tags
      })
      .then( async ({ data }) => {
        await redis.del('dataMovies')
        return data.result
      })
      .catch(err => {
        console.error(err); 
      })
    },

    editMovie(_, args) {
      console.log('masuk')
      console.log('>>>>>', args)
      const {
        id,
        title,
        overview,
        poster_path,
        popularity, tags 
      } = args

      return axios
      .put(moviesUrl + moviesEnd + id , {
        title,
        overview,
        poster_path,
        popularity, tags
      }).then( async ({ data }) => {
        await redis.del('dataMovies')
        if (data.result === null) {
          console.log('data gak ada')
        }
        return data.result
      }).catch(err => {
        
        console.error(err); 
      })
    },

    deleteMovie(_, args) {
      const { id } = args
      return axios
      .delete(moviesUrl + moviesEnd + id)
      .then( async ({ data }) => {
        await redis.del('dataMovies')
        if (data.result === null) {
          console.log('data gak ada')
        }
        return data.result
      }).catch(err => {
        console.error(err); 
      })
    },


    // Tv Series
    addSeries(_, args) {
      const { title, overview, poster_path, popularity, tags } = args

      return axios.post(seriesUrl + seriesEnd, {
        title, overview, poster_path, popularity, tags
      })
      .then( async ({ data }) => {
        await redis.del('dataSeries')
        return data.result
      })
      .catch(err => {
        console.error(err); 
      })
    },

    editSeries(_, args) {
      const {
        id,
        title,
        overview,
        poster_path,
        popularity, tags 
      } = args

      return axios
      .put(seriesUrl + seriesEnd + id , {
        title,
        overview,
        poster_path,
        popularity, tags
      }).then( async ({ data }) => {
        await redis.del('dataSeries')
        if (data.result === null) {
          console.log('data gak ada')
        }
        return data.result
      }).catch(err => {
        console.error(err);
      })
    },

    deleteSeries(_, args) {
      const { id } = args
      return axios
      .delete(seriesUrl + seriesEnd + id)
      .then( async ({ data }) => {
        await redis.del('dataSeries')
        if (data.result === null) {
          console.log('data gak ada')
        }
        return data.result
      }).catch(err => {
        console.error(err); 
      })
    }
  }

};