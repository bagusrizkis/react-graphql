const router = require('express').Router()
const { 
  MainController,
  SeriesController,
  MovieController 
} = require('../controllers')

router.get('/', MainController.getAllServices)

router.get('/mv', MovieController.getAllMoviesData)
router.get('/mv/:id', MovieController.getMovieDetail)
router.post('/mv', MovieController.addMovieData)
router.put('/mv/:id', MovieController.editMovieData)
router.delete('/mv/:id', MovieController.deleteMovieData)

router.get('/tv', SeriesController.getAllSeriesData)
router.get('/tv/:id', SeriesController.getSeriesDetail)
router.post('/tv', SeriesController.addSeriesData)
router.put('/tv/:id', SeriesController.editSeriesData)
router.delete('/tv/:id', SeriesController.deleteSeriesData)

module.exports = router