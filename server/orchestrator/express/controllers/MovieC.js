const axios = require('axios')
const redis = require('../config/redis.config')
const { moviesUrl } = require('../config/services.url')

class MovieControlller {
  static async getAllMoviesData(req, res) {
    const cacheMovies = await redis.get('dataMovies')

    if (cacheMovies) {
      return res
      .status(200)
      .json(JSON.parse(cacheMovies))
    } else if (!cacheMovies) {
      axios
        .get(moviesUrl + '/movies')
        .then(async ({ data }) => {
          await redis.set('dataMovies', JSON.stringify(data))
          return res
            .status(200)
            .json(data)
        })
        .catch(err =>
          console.log('error', err)
        )
    }
  }

  static async addMovieData (req, res) {
    axios
      .post(moviesUrl + '/movies', {
        title: req.body.title,
        overview: req.body.overview,
        poster_path: req.body.poster_path,
        popularity: req.body.popularity,
        tags: req.body.tags
      })
      .then(async ({ data }) => {
        console.log(data)
        const cacheMovies = JSON.parse(await redis.get('dataMovies'))
        if (cacheMovies) {
          const newCache = cacheMovies.result.concat(data.result)
          await redis.set('dataMovies', JSON.stringify({...cacheMovies, result: newCache}))
        }
        return res
          .status(201)
          .json(data)
      })
      .catch(err =>
        res
          .status(400)
          .json(err)
      )
  }

  static async getMovieDetail (req, res) {
    const { id } = req.params
    const cacheMovies = JSON.parse(await redis.get('dataMovies')).result
    const dataFinder = cacheMovies.filter(mv =>
      mv['_id'] === id
    )[0]

    if (!dataFinder) {
      axios
      .get(moviesUrl + '/movies/' + id)
      .then(async ({ data }) => {
        await redis.set('dataMovies_' + id, JSON.stringify(data))
      })
      .catch((err) => {
        console.log('findDetail', err)
        return res.status(404).json({
          message: 'data not found',
          result: null
        })
      })
    } else {
      return res.status(200).json({
        message: 'success get movie details',
        result: dataFinder
      })
    }

    const cacheMovieDetail = JSON.parse(await redis.get('dataMovies_' + id))
    return res.status(200).json({
      message: 'success get movie details',
      result: cacheMovieDetail.result
    })
  }

  static async editMovieData (req, res) {
    const { id } = req.params
    axios
      .put(moviesUrl + '/movies/' + id , {
        title: req.body.title,
        overview: req.body.overview,
        poster_path: req.body.poster_path,
        popularity: req.body.popularity,
        tags: req.body.tags
      })
      .then(async ({ data }) => {
        const cacheMovies = JSON.parse(await redis.get('dataMovies'))
        const newCache = cacheMovies.result.map(mv => {
          if (mv['_id'] === id) {
            mv.title = data.result.title,
            mv.overview = data.result.overview,
            mv.poster_path = data.result.poster_path,
            mv.popularity = data.result.popularity,
            mv.tags = data.result.tags
          }
          return mv
        })
        await redis.set('dataMovies', JSON.stringify({...cacheMovies, result: newCache}))
        if (data.result === null) {
          return res
            .status(404)
            .json(data)
        }
        return res
          .status(201)
          .json(data)
      })
      .catch(err => {
        return res
          .status(err.response.status)
          .json({
            message: err.message
          })
      })
  }

  static async deleteMovieData (req, res) {
    const { id } = req.params
    axios
      .delete(moviesUrl + '/movies/' + id)
      .then(async ({ data }) => {
        const cacheMovies = JSON.parse(await redis.get('dataMovies'))
        const newCache = cacheMovies.result.filter(mv => 
          mv["_id"] !== id
        )
        await redis.set('dataMovies', JSON.stringify({...cacheMovies, result: newCache}))
        return res
          .status(200)
          .json(data)
      })
      .catch(err => {
        console.error(err);
      })
  }
}

module.exports = MovieControlller