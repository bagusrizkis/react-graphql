const axios = require('axios')
const redis = require('../config/redis.config')
const { seriesUrl } = require('../config/services.url')

class SeriesControlller {
  static async getAllSeriesData(req, res) {
    const cacheSeries = await redis.get('dataSeries')

    if (cacheSeries) {
      return res
        .status(200)
        .json(JSON.parse(cacheSeries))
    }
    return axios
      .get(seriesUrl + '/series')
      .then(async ({ data }) => {
        await redis.set('dataSeries', JSON.stringify(data))
        res
          .status(200)
          .json(data)
      })
      .catch(err =>
        console.log('error', err)
      )
  }

  static async getSeriesDetail(req, res) {
    const { id } = req.params
    const cacheSeries = JSON.parse(await redis.get('dataSeries'))
    const dataFinder = cacheSeries.result.filter(tv =>
      tv['_id'] === id
    )[0]

    if (!dataFinder) {
      axios
        .get(seriesUrl + '/series/' + id)
        .then(async ({ data }) => {
          await redis.set('dataSeries_' + id, JSON.stringify(data))
        })
        .catch((err) => {
          console.log('findDetail', err)
          return res.status(404).json({
            message: 'data not found',
            result: null
          })
        })
    } else {
      return res.status(200).json({
        message: 'success get series details',
        result: dataFinder
      })
    }

    const cacheMovieDetail = JSON.parse(await redis.get('dataMovies_' + id))
    return res.status(200).json({
      message: 'success get series details',
      noteWarn: "this data doesn't exist on chaceSeries",
      result: cacheMovieDetail.result
    })
  }

  static async addSeriesData(req, res) {
    axios
      .post(seriesUrl + '/series', {
        title: req.body.title,
        overview: req.body.overview,
        poster_path: req.body.poster_path,
        popularity: req.body.popularity,
        tags: req.body.tags
      })
      .then(async ({ data }) => {
        const cacheSeries = JSON.parse(await redis.get('dataSeries'))
        if (cacheSeries) {
          const newCache = cacheSeries.result.concat(data.result)
          await redis.set('dataSeries', JSON.stringify({ ...cacheSeries, result: newCache }))
        }
        return res
          .status(201)
          .json(data)
      })
      .catch(err => {
        return res
          .status(400)
          .json(err)
      })
  }

  static async editSeriesData(req, res) {
    const { id } = req.params
    axios
      .put(seriesUrl + '/series/' + id, {
        title: req.body.title,
        overview: req.body.overview,
        poster_path: req.body.poster_path,
        popularity: req.body.popularity,
        tags: req.body.tags
      })
      .then(async ({ data }) => {
        const cacheSeries = JSON.parse(await redis.get('dataSeries'))
        const newCache = cacheSeries.result.map(tv => {
          if (tv['_id'] === id) {
            tv.title = data.result.title,
              tv.overview = data.result.overview,
              tv.poster_path = data.result.poster_path,
              tv.popularity = data.result.popularity,
              tv.tags = data.result.tags
          }
          return tv
        })
        await redis.set('dataSeries', JSON.stringify({ ...cacheSeries, result: newCache }))
        return res
          .status(201)
          .json(data)
      })
      .catch(err => {
        res
          .status(400)
          .json(err)
      }
      )
  }

  static async deleteSeriesData(req, res) {
    const { id } = req.params
    axios
      .delete(seriesUrl + '/series/' + id)
      .then(async ({ data }) => {
        const cacheSeries = JSON.parse(await redis.get('dataSeries'))
        const newCache = cacheSeries.result
          .filter(tv =>
            tv["_id"] !== id
          )

        await redis
          .set('dataSeries',
            JSON.stringify({ ...cacheSeries, result: newCache })
          )

        return res
          .status(200)
          .json(data)
      })
      .catch(err => {
        console.error(err);
      })
  }

}

module.exports = SeriesControlller