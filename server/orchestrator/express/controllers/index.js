const axios = require('axios')
const redis = require('../config/redis.config')
const { moviesUrl, seriesUrl } = require('../config/services.url')

class MainController {
  static async getAllServices (req, res) {
    const cacheMovies = await redis.get('dataMovies')
    const cacheSeries = await redis.get('dataSeries')

    if (!cacheMovies && !cacheSeries) {
      await axios
      .get(moviesUrl + '/movies')
      .then(async ({ data }) => {
        await redis.set('dataMovies', JSON.stringify(data))
      })
      .catch(err =>
        console.log('error', err)
      )
      await axios
      .get(seriesUrl + '/series')
      .then(async ({ data }) => {
        await redis.set('dataSeries', JSON.stringify(data))
      })
      .catch(err =>
        console.log('error', err)
      )
    }
    return res.status(200).json({
      movies: JSON.parse(cacheMovies),
      series: JSON.parse(cacheSeries)
    })
  }
}

module.exports = {
  MainController,
  MovieController: require('./MovieC'),
  SeriesController: require('./SeriesC')
}